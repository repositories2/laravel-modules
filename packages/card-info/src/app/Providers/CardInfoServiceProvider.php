<?php

namespace Trello\CardInfo\App\Providers;

use Illuminate\Support\ServiceProvider;

class CardInfoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'cardinfo');

        //cardinfo: Sử dụng alias để chỉ định view của package nào
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'cardinfo');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        //__DIR__: Providers folder
        // dd(scandir(__DIR__. '/../../routes'));
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/../../routes/api.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../../config/cardinfo.php' => config_path('cardinfo.php'),
            ], 'cardinfo');

            // Publishing the views.
            /*$this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/cardinfo'),
            ], 'views');*/

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/cardinfo'),
            ], 'assets');*/

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/cardinfo'),
            ], 'lang');*/

            // Registering package commands.
            // $this->commands([]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../../config/cardinfo.php', 'cardinfo');
    }
}
