<?php

namespace Trello\CardInfo\App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return response()->json(['message' => 'Hello packages']);
        }
        return view('cardinfo::welcome.index', ['message' => 'Hello packages']);
    }
}
