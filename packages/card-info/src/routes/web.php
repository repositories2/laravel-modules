<?php

use Illuminate\Support\Facades\Route;
use Trello\CardInfo\App\Http\Controllers\WelcomeController;

Route::get('hello', [WelcomeController::class, 'index']);
