# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/frontend/adtech.svg?style=flat-square)](https://packagist.org/packages/frontend/adtech)
[![Total Downloads](https://img.shields.io/packagist/dt/frontend/adtech.svg?style=flat-square)](https://packagist.org/packages/frontend/adtech)
![GitHub Actions](https://github.com/frontend/adtech/actions/workflows/main.yml/badge.svg)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require frontend/adtech
```

## Usage

```php
// Usage description here
```

### Testing

```bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email camnh@hanu.edu.vn instead of using the issue tracker.

## Credits

-   [camnh](https://github.com/frontend)
-   [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).
